#include <iostream>
#include <algorithm>
#include <cmath>
#include "geometry.h"

Rectangle Rectangle::commonPart(Rectangle p1, Rectangle p2)
{
    double x = std::max(p1.getX(), p2.getX());
    double y = std::max(p1.getY(), p2.getY());
    double width = std::min(p1.getX() + p1.getWidth(), p2.getX() + p2.getWidth()) - x;
    double height = std::min(p1.getY() + p1.getHeight(), p2.getY() + p2.getHeight()) - y;

    Rectangle p(x, y, width, height);

    return p;
}
bool Rectangle::checkIntersecting(Rectangle p2)
{
    bool xOverlap = Rectangle::inRange(this->x, p2.getX(), p2.getX() + p2.getWidth()) ||
                    Rectangle::inRange(p2.getX(), this->x, this->x + this->width);

    bool yOverlap = Rectangle::inRange(this->y, p2.getY(), p2.getY() + p2.getHeight()) ||
                    Rectangle::inRange(p2.getY(), this->y, this->y + this->height);

    return xOverlap && yOverlap;
}
inline bool Rectangle::inRange(double value, double min, double max)
{
    return (value > min) && (value < max);
}
void Rectangle::writeData()
{
    std::cout << this->x << " " << this->y << " " << this->width << " " << this->height << std::endl;
}
Rectangle Rectangle::loadData()
{
    double x, y, width, height;
    std::cin >> x >> y >> width >> height;
    Rectangle p(x, y, width, height);
    return p;
}
void Rectangle::ifNotInTheRange()
{
    std::cout << "0 0 0 0" << std::endl;
}
