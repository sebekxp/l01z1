#ifndef GEOMETRY_H
#define GEOMETRY_H

class Rectangle
{
  public:
    Rectangle(double x, double y, double width, double height)
        : x(x), y(y), height(height), width(width) {} // Constructor which initiates variables

    inline double getX() { return x; }           // Return value of x
    inline double getY() { return y; }           // Return value of y
    inline double getWidth() { return width; }   // Return value of width
    inline double getHeight() { return height; } // Return value of height

    static Rectangle loadData();                       // Load the object data
    static Rectangle commonPart(Rectangle, Rectangle); // Calculate the common part
    static void ifNotInTheRange();                     // If the rectangles do not intersect

    bool checkIntersecting(Rectangle); // Check if the objects intersect
    void writeData();                  // Shows data about the resulting rectangle

  private:
    const double x;                       // The x-value of the bottom left edge of the rectangle
    const double y;                       // The y-value of the bottom left edge of the rectangle
    const double height;                  // Width of the rectangle
    const double width;                   // Height of the rectangle
    bool inRange(double, double, double); // Check if the values are in range
};

#endif //GEOMETRY_H