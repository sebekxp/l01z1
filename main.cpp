#include <iostream>
#include "geometry.h"

int main()
{
    Rectangle p1 = Rectangle::loadData();
    Rectangle p2 = Rectangle::loadData();

    if (p1.checkIntersecting(p2))
    {
        Rectangle p3 = Rectangle::commonPart(p1, p2);
        p3.writeData();
    }
    else
        Rectangle::ifNotInTheRange();

    return 0;
}
